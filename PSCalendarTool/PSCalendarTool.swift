//
//  File.swift
//  PSCalendarTool
//
//  Created by Philipp Seitz on 13.12.15.
//  Copyright © 2015 Philipp Seitz. All rights reserved.
//

import Foundation

public struct PSCalendarEvent: Equatable, Hashable{
    
    public var uid: String?
    public var startDate: NSDate?
    public var endDate: NSDate?
    public var description: String?
    public var location: String?
    public var summary: String?
    public var hashValue: Int{
        get{
            guard let endDate = endDate, let startDate = startDate else{
                return 0
            }
            return endDate.hash &+ startDate.hash
        }
    }
    
    static func calendarEventFromString(string: String) -> PSCalendarEvent?{
        var ret = PSCalendarEvent()
        let array = string.characters.split{$0 == "\n"}.map(String.init)
        
        var dictionary: [String: String] = [:]
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "Europe/Berlin")
        dateFormatter.dateFormat = "yyyyMMdd'T'HHmmss"
        
        for str in array{
            let splitted = str.characters.split(1, allowEmptySlices: false){ $0 == ":" }.map(String.init)
            dictionary[splitted[0]] = splitted[1]
        }
        
        guard let startDateString = dictionary[ICSvalue.startDate.rawValue], let endDateString = dictionary[ICSvalue.endDate.rawValue],  let startDate = dateFormatter.dateFromString(startDateString), let endDate = dateFormatter.dateFromString(endDateString) else{
            return nil
        }
        
        ret.summary = dictionary[ICSvalue.summary.rawValue]
        ret.location = dictionary[ICSvalue.location.rawValue]
        ret.startDate = startDate
        ret.description = dictionary[ICSvalue.description.rawValue]
        ret.endDate = endDate
        
        return ret
        
    }
    
    
    
    private enum ICSvalue: String{
        case uid = "UID"
        case begin = "BEGIN"
        case startDate = "DTSTART;TZID=Europe/Berlin"
        case created = "CREATED"
        case summary = "SUMMARY"
        case location = "LOCATION"
        case description = "DESCRIPTION"
        case endDate = "DTEND;TZID=Europe/Berlin"
    }
    
}

public func ==(lhs: PSCalendarEvent, rhs: PSCalendarEvent) -> Bool{
    return lhs.description == rhs.description && lhs.endDate?.timeIntervalSince1970 == rhs.endDate?.timeIntervalSince1970 && lhs.location == rhs.location && lhs.startDate?.timeIntervalSince1970 == rhs.startDate?.timeIntervalSince1970 && lhs.summary == rhs.summary
}

extension SequenceType where Generator.Element == PSCalendarEvent {
    func getGroupedCalendar(date: NSDate? = nil) -> OrderedDictionary<String, [PSCalendarEvent]> {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        var groupedPSCalendarEvents = OrderedDictionary<String, [PSCalendarEvent]>()
        
        for calEv in self {
            if let startDate = calEv.startDate, let endDate = calEv.endDate where endsBeforeDate(date, endDate: endDate) {
                let key = dateFormatter.stringFromDate(startDate)
                if (groupedPSCalendarEvents[key] == nil) {
                    groupedPSCalendarEvents[key] = [PSCalendarEvent](arrayLiteral: calEv)
                } else {
                    groupedPSCalendarEvents[key]?.append(calEv)
                }
            }
        }
        
        return groupedPSCalendarEvents
    }
    
    private func endsBeforeDate(date: NSDate?, endDate: NSDate) -> Bool {
        return endDate.timeIntervalSince1970 > date?.timeIntervalSince1970
    }
    
}