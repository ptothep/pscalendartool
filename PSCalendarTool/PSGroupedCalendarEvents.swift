//
//  PSGroupedCalendarEvents.swift
//  PSCalendarTool
//
//  Created by Philipp Seitz on 09.03.16.
//  Copyright © 2016 Philipp Seitz. All rights reserved.
//

import Foundation

public typealias PSGroupedCalendarEvents = OrderedDictionary<String, [PSCalendarEvent]>


public func !=(lhs: PSGroupedCalendarEvents, rhs: PSGroupedCalendarEvents) -> Bool{
    if lhs.count != rhs.count{
        return true
    }
    for key in lhs.keys{
        if let l = lhs[key], let r = rhs[key]{
            if l != r{
                return true
            }
        }else{
            return true
        }
    }
    return false
}

public func /(left: PSGroupedCalendarEvents, right: PSGroupedCalendarEvents)->PSGroupedCalendarEvents{
    var ret = PSGroupedCalendarEvents()
    for leftValue in left.values{
        
        if let rightSub = right[leftValue.0]{
            let rightSubSet = Set(rightSub)
            var arry = [PSCalendarEvent]()
            for a in leftValue.1{
                if !rightSubSet.contains(a){
                    arry.append(a)
                }
            }
            if arry.count > 0{
                ret[leftValue.0] = arry
            }
        }else{
            ret[leftValue.0] = leftValue.1
        }
    }
    
    ret.keys = ret.keys.sort{(first, second) -> Bool in
        return first < second
    }
    
    return ret
}

public func ==(left: PSGroupedCalendarEvents, right: PSGroupedCalendarEvents)->Bool{
    if(left.count != right.count){
        return false;
    }
    for a in left.keys{
        guard let rightvalue = right[a] else{
            return false;
        }
        if(rightvalue != left[a]!){
            return false;
        }
    }
    return true;
}

public func +(left: PSGroupedCalendarEvents, right: PSGroupedCalendarEvents)->PSGroupedCalendarEvents{
    var ret = left
    
    for rightValue in right.values{
        if(!ret.keys.contains(rightValue.0)){
            ret[rightValue.0] = rightValue.1
        }else{
            for value in rightValue.1{
                if(!ret[rightValue.0]!.contains(value)){
                    ret[rightValue.0]!.append(value)
                    ret[rightValue.0] = ret[rightValue.0]!.sort({ (left, right) -> Bool in
                        return left.startDate?.timeIntervalSince1970 < right.startDate?.timeIntervalSince1970
                    })
                }
            }
        }
    }
    
    ret.keys = ret.keys.sort { (first, second) -> Bool in
        return first < second
    }
    
    return ret
}