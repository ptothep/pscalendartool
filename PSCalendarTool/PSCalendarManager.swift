//
//  PSCalendarManager.swift
//  PSCalendarTool
//
//  Created by Philipp Seitz on 15.12.15.
//  Copyright © 2015 Philipp Seitz. All rights reserved.
//

import Foundation

public typealias PSCalendarCallback = ((url: NSURL, error: NSError?, result: PSGroupedCalendarEvents?)->())

public enum PSDataOrigin{
    case FileSystem
    case Cache
    case Online
}

public class PSCalendarManager{
    public static let sharedInstance = PSCalendarManager()
    //    private var cache = [String: PSGroupedCalendarEvents]()
    private var documentDirectory: NSString?
    private var callBackQueue = [String: [PSCalendarCallback]]()
    
    init(){
        documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first
        if documentDirectory == nil{
            print("PSCalendarManager: Warning! Offline Modus not available!")
        }
    }
    
    private func offlineDataFromPath(path: String)-> NSData?{
        
        if let dir = documentDirectory{
            let p = dir.stringByAppendingPathComponent(path)
            return NSData(contentsOfFile: p)
        }
        return nil
        
    }
    
    public func offlineDataAvailable(url: NSURL) -> Bool{
        guard let urlString = reduceString(url.description) else{
            return false
        }
        //        if cache[urlString] != nil{
        //            return true
        //        }
        if let dir = documentDirectory{
            let p = dir.stringByAppendingPathComponent(urlString)
            let fm = NSFileManager()
            return fm.isReadableFileAtPath(p)
        }
        return false
    }
    
    private func saveDataToPath(path: String, data: NSData){
        if let dir = documentDirectory{
            let p = dir.stringByAppendingPathComponent(path)
            data.writeToFile(p, atomically: false)
        }
    }
    
    private func sortedCalendarEventsFromString(string: String) -> [PSCalendarEvent] {
        let calendarEvents = ICSParser.parseICStoCalendarEvents(string)
        return calendarEvents.sort {
            $0.startDate?.timeIntervalSince1970 < $1.startDate?.timeIntervalSince1970
        }
    }
    
    
    public func loadOfflineAsync(url: NSURL, callBack: PSCalendarCallback){
        //TODO: Error handling!
        guard let urlString = reduceString(url.description) else{
            //Error Link
            print("Url error: \(url.description)")
            callBack(url: url, error: NSError(domain:"Url error",code: 404, userInfo: [NSObject : AnyObject]()),  result: nil)
            return
        }
        
        //Drive
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            
            if let data = self.offlineDataFromPath(urlString){
                print("load form disk")
                let content = NSString(data: data, encoding: NSUTF8StringEncoding)
                if let str = content as String?{
                    let calEv = self.sortedCalendarEventsFromString(str)
                    let groupedCalEv = calEv.getGroupedCalendar()
                    //                    self.cache[urlString] = groupedCalEv
                    callBack(url: url, error: nil, result: groupedCalEv)
                    return
                }else{
                    //TODO
                    callBack(url: url, error: nil, result: nil)
                }
                
            }else{
                //TODO
                print("no offline Data available")
                callBack(url: url, error: NSError(domain:"Data",code: 404, userInfo: [NSObject : AnyObject]()), result: nil)
            }
        })
        
        
    }
    
    public func loadOnlineAsync(url: NSURL, callBack: PSCalendarCallback){
        //TODO Error handling!
        guard let urlString = reduceString(url.description) else{
            //Error Link
            print("Url error: \(url.description)")
            callBack(url: url, error:  NSError(domain:"Url error",code: 404, userInfo: [NSObject : AnyObject]()),  result: nil)
            return
        }
        //Online
        if var c = callBackQueue[urlString]{
            print("CallBackQueue +1")
            c.append(callBack)
            callBackQueue[urlString] = c
            return
        }
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        //SPÄTER RAUSNEHMEN NICHT VERGESSEN !!11elf
//        NSURLCache.sharedURLCache().removeAllCachedResponses()
        let session = NSURLSession(configuration: configuration)
        
        let task = session.dataTaskWithURL(url){ data, _, error in
            if let data = data where error == nil{
                
                self.saveDataToPath(self.reduceString(url.description)!, data: data)
                let content = NSString(data: data, encoding: NSUTF8StringEncoding)
                if let str = content as String? {
                    let calEv = self.sortedCalendarEventsFromString(str)
                    print("fire")
                    callBack(url: url, error: nil, result: calEv.getGroupedCalendar())
                }else{
                    callBack(url: url, error:  NSError(domain:"Data Parsing Error / Corrupt Calendar Data",code: 404, userInfo: [NSObject : AnyObject]()), result: nil)
                }
            }else{
                callBack(url: url, error: error, result: nil)
            }
        }
        task.resume()
    }
    
    public func filterCalendarByDate(calendar: PSGroupedCalendarEvents, date: NSDate) -> PSGroupedCalendarEvents{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        let dateString = dateFormatter.stringFromDate(date)
        
        var r = calendar.filter { (string) -> Bool in
            return string >= dateString
        }
        
        r[dateString] = r[dateString]?.filter({ (event) -> Bool in
            event.endDate?.timeIntervalSinceDate(date) > 0
        })
        
        if(r[dateString]?.count == 0){
            r[dateString] = nil
        }
        
        return r
        
    }
    
    
    
    public  func reduceString(string: String) -> String? {
        
        let reverse = String(string.characters.reverse())
        guard let index = reverse.characters.indexOf("/") else {
            return nil
        }
        let short = reverse.substringToIndex(index)
        let dataName = String(short.characters.reverse())
        
        guard dataName.hasSuffix(".ics") else{
            return nil
        }
        
        return dataName
        
    }
}