//
// Created by Philipp Seitz on 05.12.15.
// Copyright (c) 2015 Philipp Seitz. All rights reserved.
//

import Foundation

public class ICSParser {
    public static func parseICStoCalendarEvents(var string: String) -> [PSCalendarEvent] {
        var returnEvents = [PSCalendarEvent]()
        var range: Range<String.Index>?
        var plainArray = [String]()

        repeat {

            range = string.rangeOfString("(?s)BEGIN:VEVENT.*?END:VEVENT", options: .RegularExpressionSearch)

            if range != nil {
                plainArray.append(string.substringWithRange(range!))
                string.removeRange(range!)
            }

        } while range != nil

        for s in plainArray {
            if let ev = PSCalendarEvent.calendarEventFromString(s){
                returnEvents.append(ev)
            }
            
        }

        return returnEvents
    }
}
